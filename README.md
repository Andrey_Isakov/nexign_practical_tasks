To start the container with home_assistant use the command:

docker-compose up -d

To see the result follow the link:

localhost:8123
